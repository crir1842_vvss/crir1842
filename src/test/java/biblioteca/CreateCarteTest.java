package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class CreateCarteTest {
    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;

    @Before
    public void init() {
        repo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(repo);
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws Exception{
        Carte carte = Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri,mihai");
        ctrl.adaugaCarte(carte);
        assertEquals(true, ctrl.exists(carte));
    }

    @Test
    public void test2() throws Exception{
        Carte carte = Carte.getCarteFromString("Povesti;Mihai Eminescu;1973;Corint;povesti,povestiri,mihai,fsdf,sdfsdf,sdfsdf,sdfsd");
        ctrl.adaugaCarte(carte);
        assertEquals(true, ctrl.exists(carte));
    }

    @Test
    public void test3() throws Exception{
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Numarul de cuvinte cheie nu este intre 3 si 7!");
        Carte carte = Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,mihai");
        ctrl.adaugaCarte(carte);
    }

    @Test
    public void test4() throws Exception{
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Numarul de cuvinte cheie nu este intre 3 si 7!");
        Carte carte = Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,mihai,sdffsd,sdfdsf,sdfsdf,sdfsdf,sdfsdf,fds");
        ctrl.adaugaCarte(carte);
    }

    @Test
    public void test5() throws Exception{
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Autor invalid!");
        Carte carte = Carte.getCarteFromString("Povesti;;1973;Corint;povesti,mihai,sdffsd,sdfdsf,sdfsdf,sdfsdf");
        ctrl.adaugaCarte(carte);
    }

    @Test
    public void test6() throws Exception{
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Numarul de autori nu este intre 1 si 3!");
        Carte carte = Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga,Alexandru Lapusneanu;1973;Corint;povesti,mihai,povestiri");
        ctrl.adaugaCarte(carte);
    }

    @Test
    public void test7() throws Exception{
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("An aparitie invalid!");
        Carte carte = Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;fdsfsd;Corint;povesti,mihai,povestiri");
        ctrl.adaugaCarte(carte);
    }
}

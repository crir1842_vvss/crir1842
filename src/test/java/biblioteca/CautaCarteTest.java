package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CautaCarteTest {
    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;

    @Before
    public void init() {
        repo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(repo);
        repo.stergeCarti();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws Exception{
        List<Carte> rezultatCarti = ctrl.cautaCarte("autor");
        assertEquals(0, rezultatCarti.size());
    }

    @Test
    public void test2() throws Exception{
        Carte carte = Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri,asdas");
        ctrl.adaugaCarte(carte);
        List<Carte> rezultatCarti = ctrl.cautaCarte("Eminescu");
        assertEquals(carte, rezultatCarti.get(0));
    }
}

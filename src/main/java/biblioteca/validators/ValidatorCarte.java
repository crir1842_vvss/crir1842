package biblioteca.validators;

import biblioteca.model.Carte;

public class ValidatorCarte {
    public static void validateCarte(Carte c)throws Exception{
        if(c.getCuvinteCheie()==null){
            throw new Exception("Lista cuvinte cheie vida!");
        }

        for(String s:c.getCuvinteCheie()){
            if(!Validator.isOKString(s))
                throw new Exception("Cuvant cheie invalid!");
        }

        if(c.getCuvinteCheie().size() < 3 || c.getCuvinteCheie().size() > 7)
            throw new Exception("Numarul de cuvinte cheie nu este intre 3 si 7!");



        if(c.getReferenti()==null){
            throw new Exception("Lista autori vida!");
        }

        if(c.getReferenti().size() < 1 || c.getReferenti().size() > 3)
            throw new Exception("Numarul de autori nu este intre 1 si 3!");

        for(String s:c.getReferenti()){
            if(!Validator.isOKString(s))
                throw new Exception("Autor invalid!");
        }


        if(!Validator.isOKString(c.getTitlu()))
            throw new Exception("Titlu invalid!");

        if(!Validator.isOKString(c.getEditura()))
            throw new Exception("Editura invalida!");

        if(!Validator.isNumber(c.getAnAparitie()))
            throw new Exception("An aparitie invalid!");
    }
}
